package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import java.util.Calendar;

@Entity
@Table(name = "log_asdos")
@Data
@NoArgsConstructor
public class LogAsdos {

    @Id
    @Column(name = "idLog")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private int idLog;

    @Column(name = "startTime", columnDefinition = "timestamp")
    private Calendar start;

    @Column(name = "endTime", columnDefinition = "timestamp")
    private Calendar end;

    @Column(name = "deskripsi", columnDefinition = "text")
    private String desc;

    @ManyToOne
    @JoinColumn(name="mahasiswa_id", nullable = false)
    private Mahasiswa mahasiswa;

    public LogAsdos(Calendar start, Calendar end, String desc) {
        this.start = start;
        this.end = end;
        this.desc = desc;
    }

    public void setIdLog(int idLog) {
        this.idLog = idLog;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Mahasiswa getMahasiswa() {
        return this.mahasiswa;
    }

    public Calendar getStart() { return this.start; }
    public int getDuration() {
        int ret = (int) (this.end.getTimeInMillis() - this.start.getTimeInMillis());
        return Math.max(ret, 0);
    }
}
