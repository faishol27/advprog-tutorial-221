package csui.advpro2021.tais.model;

public class LogSummary {
    private static final double FEE_HOURLY = 350.0;
    private String month;
    private double jamKerja;
    private double pembayaran;
    
    public LogSummary(String month, double jamKerja) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = FEE_HOURLY * jamKerja;
    }

    public String getMonth() { return this.month; }
    public double getJamKerja() { return this.jamKerja; }
    public double getPembayaran() { return this.pembayaran; }

}