package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.service.LogAsdosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "/logs")
public class LogAsdosController {
    @Autowired
    private LogAsdosService logService;

    @GetMapping(path = "/{npm}/summary", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getSummary(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getLogSummary(npm));
    }

    @GetMapping(path = "/{npm}/total", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getTotalMoney(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getTotalMoney(npm));
    }
    
    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListLog(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLog(npm));
    }

    @PostMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addLog(@PathVariable(value = "npm") String npm, @RequestBody LogAsdos log) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }
    
    @GetMapping(path = "/{npm}/{id}", produces = {"application/json"})
    public ResponseEntity getLog(@PathVariable(value = "npm") String npm, @PathVariable(value = "id") int id, @RequestBody LogAsdos log) {
        return ResponseEntity.ok(logService.getLogByIdLog(npm, id));
    }

    @PutMapping(path = "/{npm}/{id}", produces = {"application/json"})
    public ResponseEntity updateLog(@PathVariable(value = "npm") String npm, @PathVariable(value = "id") int id, @RequestBody LogAsdos log) {
        return ResponseEntity.ok(logService.updateLog(npm, id, log));
    }

    @DeleteMapping(path = "/{npm}/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "npm") String npm, @PathVariable(value = "id") int id) {
        logService.deleteLogByIdLog(npm, id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
