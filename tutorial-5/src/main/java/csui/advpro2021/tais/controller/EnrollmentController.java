package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/enroll")
public class EnrollmentController {
    @Autowired
    private EnrollmentService enrollService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity enrollMataKuliah(@RequestBody HashMap<String, String> bodyReq) {
        String npm = bodyReq.get("npm");
        String kodeMatkul = bodyReq.get("kodeMatkul");
        if (npm == null || kodeMatkul == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        Mahasiswa ret = enrollService.enrollMataKuliah(kodeMatkul, npm);
        if (ret == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(ret);
    }

    @GetMapping(path = "/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity listPendaftarMataKuliah(@PathVariable(value = "kodeMatkul") String kodeMatkul) {
        Iterable<Mahasiswa> pendaftar = enrollService.pendaftarMataKuliah(kodeMatkul);
        if (pendaftar == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pendaftar);
    }
}
