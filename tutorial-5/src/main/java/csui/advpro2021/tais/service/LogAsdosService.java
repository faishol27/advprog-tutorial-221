package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.LogSummary;

import java.util.ArrayList;
import java.util.HashMap;

public interface LogAsdosService {
    ArrayList<LogSummary> getLogSummary(String npm);

    HashMap<String, Double> getTotalMoney(String npm);

    Iterable<LogAsdos> getListLog(String npm);
    
    LogAsdos createLog(String npm, LogAsdos log);

    LogAsdos getLogByIdLog(String npm, int id);

    LogAsdos updateLog(String npm, int id, LogAsdos log);

    void deleteLogByIdLog(String npm, int id);
}   
