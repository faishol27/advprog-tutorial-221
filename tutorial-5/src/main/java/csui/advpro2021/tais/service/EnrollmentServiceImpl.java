package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Mahasiswa enrollMataKuliah(String kodeMatkul, String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null || !mahasiswa.isMatkulNull()) {
            return null;
        }

        MataKuliah matkul = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
        if (matkul == null) {
            return null;
        }
        mahasiswa.setMatkul(matkul);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> pendaftarMataKuliah(String kodeMatkul) {
        MataKuliah matkul = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
        if (matkul == null) {
            return null;
        }
        return mahasiswaRepository.findByMatkul(matkul);
    }
}
