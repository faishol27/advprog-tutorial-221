package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.repository.LogAsdosRepository;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.MahasiswaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import static java.util.Calendar.LONG;
import static java.util.Calendar.MONTH;
import static java.util.Locale.US;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogAsdosServiceImpl implements LogAsdosService {
    @Autowired
    private LogAsdosRepository logRepo;
    @Autowired
    private MahasiswaRepository mhsRepo;

    @Override
    public ArrayList<LogSummary> getLogSummary(String npm){
        Iterable<LogAsdos> logs = this.getListLog(npm);
        HashMap<String, Double> hourAccum = new HashMap<>();
        ArrayList<LogSummary> ret = new ArrayList<>();

        for (LogAsdos log:logs) {
            double duration = log.getDuration() / (3600.0 * 1000.0);
            String month = log.getStart().getDisplayName(MONTH, LONG, US);
            if (hourAccum.get(month) != null) {
                duration += hourAccum.get(month);
            }
            hourAccum.put(month, duration);
        }

        for (Map.Entry elm:hourAccum.entrySet()) {
            String key = elm.getKey().toString();
            LogSummary tmp = new LogSummary(key, hourAccum.get(key));
            ret.add(tmp);
        }

        return ret;
    }

    @Override
    public HashMap<String, Double> getTotalMoney(String npm){
        ArrayList<LogSummary> logs = this.getLogSummary(npm);
        double accum = 0;
        for (LogSummary log:logs) {
            accum += log.getPembayaran();
        }

        HashMap<String, Double> ret = new HashMap<>();
        ret.put("totalPayment", accum);
        return ret;
    }

    @Override
    public Iterable<LogAsdos> getListLog(String npm){
        Mahasiswa mhs = mhsRepo.findByNpm(npm);
        return logRepo.findByMahasiswa(mhs);
    }

    @Override
    public LogAsdos createLog(String npm, LogAsdos log){
        Mahasiswa mhs = mhsRepo.findByNpm(npm);
        if (mhs == null || mhs.getMatkul() == null) {
            return null;
        }
        log.setMahasiswa(mhs);
        logRepo.save(log);
        return log;
    }

    @Override
    public LogAsdos getLogByIdLog(String npm, int id){
        LogAsdos ret = logRepo.findByIdLog(id);
        if (ret == null || ret.getMahasiswa().getNpm().equals(npm) == false) {
            return null;
        }
        return ret;
    }

    @Override
    public LogAsdos updateLog(String npm, int id, LogAsdos log){
        Mahasiswa mhs = mhsRepo.findByNpm(npm);
        LogAsdos oldLog = this.getLogByIdLog(npm, id);
        if (oldLog == null) {
            return null;
        }

        log.setIdLog(id);
        log.setMahasiswa(mhs);
        logRepo.save(log);
        return log;
    }

    @Override
    public void deleteLogByIdLog(String npm, int id){
        LogAsdos log = this.getLogByIdLog(npm, id);
        logRepo.delete(log);
    }

}
