package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.Mahasiswa;

public interface EnrollmentService {
    Mahasiswa enrollMataKuliah(String kodeMatkul, String npm);
    Iterable<Mahasiswa> pendaftarMataKuliah(String kodeMatkul);    
}
