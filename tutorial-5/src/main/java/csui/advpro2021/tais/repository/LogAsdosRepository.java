package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogAsdosRepository extends JpaRepository<LogAsdos, Integer> {
    LogAsdos findByIdLog(int id);
    Iterable<LogAsdos> findByMahasiswa(Mahasiswa mahasiswa);
}
