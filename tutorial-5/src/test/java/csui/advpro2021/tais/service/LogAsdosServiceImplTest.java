package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogAsdosRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;

@ExtendWith(MockitoExtension.class)
public class LogAsdosServiceImplTest {
    @Mock
    LogAsdosRepository logRepo;

    @Mock
    MahasiswaRepository mhsRepo;

    @InjectMocks
    LogAsdosServiceImpl logService;

    private final String NPM = "1234";
    private Mahasiswa mhs;
    private MataKuliah matkul;
    private LogAsdos log;
    private LogSummary summary;

    @BeforeEach
    public void setUp() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.set(2021, 4, 3, 8, 0, 0);

        matkul = new MataKuliah("APA", "Ada Apa", "Cinta");
        mhs = new Mahasiswa(NPM, "Kapan kelar", "hufftt", "4", "4444");
        log = new LogAsdos(cal1, cal2, "aku capekkkk");
        summary = new LogSummary("January", 30);
    }

    @Test
    public void testGetLogByIdCorrect() throws Exception {
        log.setMahasiswa(mhs);
        when(logRepo.findByIdLog(1)).thenReturn(log);
        LogAsdos retTest = logService.getLogByIdLog(NPM, 1);
        Assertions.assertEquals(log.getIdLog(), retTest.getIdLog());
    }

    @Test
    public void testGetLogByIdNpmMissmatch() throws Exception {
        log.setMahasiswa(mhs);
        when(logRepo.findByIdLog(1)).thenReturn(log);
        LogAsdos retTest = logService.getLogByIdLog("1111", 1);
        Assertions.assertEquals(null, retTest);
    }
    
    @Test
    void testServiceGetListLog() {
        ArrayList<LogAsdos> retMock = new ArrayList<>();
        retMock.add(log);        
        when(logService.getListLog(NPM)).thenReturn(retMock);
        
        Iterable<LogAsdos> ret = logService.getListLog(NPM);
        assertEquals(ret, retMock);
    }

    @Test
    void testServiceUpdateLog() {
        Calendar cal = Calendar.getInstance();
        LogAsdos mockLog = new LogAsdos(cal, cal, "HAHAHAHHA");
        log.setMahasiswa(mhs);

        when(logRepo.findByIdLog(1)).thenReturn(log);
        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);
        
        LogAsdos ret = logService.updateLog(NPM, 1, mockLog);
        assertEquals(mhs, ret.getMahasiswa());
    }

    @Test
    void testServiceUpdateLogLogNotExist() {
        Calendar cal = Calendar.getInstance();
        LogAsdos mockLog = new LogAsdos(cal, cal, "uhuuuuy");
        
        LogAsdos ret = logService.updateLog(NPM, 1, mockLog);
        assertEquals(null, ret);
    }
    @Test
    void testServiceGetLogSummary() {
        ArrayList<LogAsdos> logListMock = new ArrayList<>();
        logListMock.add(log);
        logListMock.add(log);

        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);
        when(logRepo.findByMahasiswa(mhs)).thenReturn(logListMock);

        ArrayList<LogSummary> ret = logService.getLogSummary(NPM);
        assertNotEquals(null, ret);
    }

    @Test
    void testServiceGetTotalMoney() {
        ArrayList<LogAsdos> logListMock = new ArrayList<>();
        logListMock.add(log);

        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);
        when(logRepo.findByMahasiswa(mhs)).thenReturn(logListMock);

        HashMap<String, Double> ret = logService.getTotalMoney(NPM);
        assertNotEquals(null, ret.get("totalPayment"));
    }

    @Test
    void testServiceCreateLog() {
        mhs.setMatkul(matkul);
        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);
        
        LogAsdos ret = logService.createLog(NPM, log);
        assertEquals(NPM, ret.getMahasiswa().getNpm());
    }

    @Test
    void testServiceCreateLogMahasiswaNotEnrolledYet() {
        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);
        
        LogAsdos ret = logService.createLog(NPM, log);
        assertEquals(null, ret);
    }

    @Test
    void testServiceCreateLogMahasiswaNotExist() {       
        LogAsdos ret = logService.createLog(NPM, log);
        assertEquals(null, ret);
    }

    @Test
    void testServiceDeleteLog() {
        LogAsdosServiceImpl logSpy = spy(logService);

        mhs.setMatkul(matkul);
        when(mhsRepo.findByNpm(NPM)).thenReturn(mhs);

        logSpy.createLog(NPM, log);
        logSpy.deleteLogByIdLog(NPM, log.getIdLog());
        Assertions.assertEquals(null, logSpy.getLogByIdLog(NPM, log.getIdLog()));
    }
}
