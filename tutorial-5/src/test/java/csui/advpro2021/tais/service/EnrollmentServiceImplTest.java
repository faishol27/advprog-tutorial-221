package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EnrollmentServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepo;

    @Mock
    private MataKuliahRepository matkulRepo;

    @InjectMocks
    private EnrollmentServiceImpl enrollmentService;

    private Mahasiswa mahasiswa;
    private MataKuliah matkul;
    private final String kodeMatkul = "APA";
    private final String npm = "1234";

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa(npm, "Hayati Lelah", "Entah Apa", "4", "1234");
        matkul = new MataKuliah(kodeMatkul, "Analisis Pacar Aku", "Cinta");
    }
    
    @Test
    public void testEnrollMatkulCorrect() throws Exception {
        when(mahasiswaRepo.findByNpm(npm)).thenReturn(mahasiswa);
        when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(matkul);

        Mahasiswa resultTest = enrollmentService.enrollMataKuliah(kodeMatkul, npm);
        Assertions.assertEquals(kodeMatkul, resultTest.getMatkul().getKodeMatkul());
    }
    
    @Test
    public void testEnrollMatkulMahasiswaNotExist() throws Exception {
        when(mahasiswaRepo.findByNpm(npm)).thenReturn(null);
        lenient().when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(matkul);

        Mahasiswa resultTest = enrollmentService.enrollMataKuliah(kodeMatkul, npm);
        Assertions.assertEquals(null, resultTest);
    }

    @Test
    public void testEnrollMatkulMahasiswaAlreadyEnrolled() throws Exception {
        mahasiswa.setMatkul(matkul);
        when(mahasiswaRepo.findByNpm(npm)).thenReturn(mahasiswa);
        lenient().when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(matkul);

        Mahasiswa resultTest = enrollmentService.enrollMataKuliah(kodeMatkul, npm);
        Assertions.assertEquals(null, resultTest);
    }

    @Test
    public void testEnrollMatkulMataKuliahNotExist() throws Exception {
        when(mahasiswaRepo.findByNpm(npm)).thenReturn(mahasiswa);
        when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(null);

        Mahasiswa resultTest = enrollmentService.enrollMataKuliah(kodeMatkul, npm);
        Assertions.assertEquals(null, resultTest);
    }

    @Test
    public void testPendaftarMatkulCorrect() throws Exception {
        ArrayList<Mahasiswa> expRet = new ArrayList<>();
        expRet.add(mahasiswa);

        when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(matkul);
        when(mahasiswaRepo.findByMatkul(matkul)).thenReturn(expRet);

        Iterable<Mahasiswa> resultTest = enrollmentService.pendaftarMataKuliah(kodeMatkul);
        Assertions.assertEquals(expRet, resultTest);
    }

    @Test
    public void testPendaftarMatkulMataKuliahNotExist() throws Exception {
        when(matkulRepo.findByKodeMatkul(kodeMatkul)).thenReturn(null);
        Iterable<Mahasiswa> resultTest = enrollmentService.pendaftarMataKuliah(kodeMatkul);
        Assertions.assertEquals(null, resultTest);
    }
}
