package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MataKuliahServiceImplTest {
    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private MataKuliahServiceImpl matkulService;

    private MataKuliah matkul;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah();
        matkul.setKodeMatkul("ADVPROG");
        matkul.setNama("Advanced Programming");
        matkul.setProdi("Ilmu Komputer");
    }

    @Test
    void testServiceGetListMataKuliah() {
        Iterable<MataKuliah> listMataKuliah = mataKuliahRepository.findAll();
        lenient().when(matkulService.getListMataKuliah()).thenReturn(listMataKuliah);
        Iterable<MataKuliah> listMataKuliahResult = matkulService.getListMataKuliah();
        Assertions.assertIterableEquals(listMataKuliah, listMataKuliahResult);
    }

    @Test
    void testServiceGetMataKuliah() {
        lenient().when(matkulService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        MataKuliah resultMatkul = matkulService.getMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceCreateMataKuliah() {
        lenient().when(matkulService.createMataKuliah(matkul)).thenReturn(matkul);
        MataKuliah resultMatkul = matkulService.createMataKuliah(matkul);
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceUpdateMataKuliah() {
        matkulService.createMataKuliah(matkul);
        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);
        MataKuliah expectedMatkul = matkul;
        expectedMatkul.setNama(namaMatkul);
        MataKuliah resultMatkul = matkulService.updateMataKuliah(matkul.getKodeMatkul(), matkul);
        Assertions.assertEquals(expectedMatkul.getNama(), resultMatkul.getNama());
    }

    @Test
    void testServiceDeleteMataKuliah() {
        matkulService.createMataKuliah(matkul);
        matkulService.deleteMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(null, matkulService.getMataKuliah(matkul.getKodeMatkul()));
    }
}
