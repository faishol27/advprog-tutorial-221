package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.LogAsdos;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.service.LogAsdosServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogAsdosController.class)
public class LogAsdosControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogAsdosServiceImpl logService;

    private Mahasiswa mhs;
    private LogAsdos log;
    private LogSummary logSummary;

    @BeforeEach
    public void setUp(){
        Calendar cal = Calendar.getInstance();
        mhs = new Mahasiswa("1234", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        log = new LogAsdos(cal, cal, "AIIHHHH");
        logSummary = new LogSummary("January", 20);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerLogsGetSummary() throws Exception{
        ArrayList<LogSummary> mockSummary = new ArrayList<>();
        mockSummary.add(logSummary);
        when(logService.getLogSummary("1234")).thenReturn(mockSummary);

        mvc.perform(get("/logs/1234/summary").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].month").value("January"));
    }
}
