package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.EnrollmentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = EnrollmentController.class)
public class EnrollmentControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private EnrollmentServiceImpl enrollService;

    private Mahasiswa mhs;
    private MataKuliah matkul;

    @BeforeEach
    public void setUp(){
        mhs = new Mahasiswa("1234", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        matkul = new MataKuliah("APA", "INI", "SIKSAAN?");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerEnrollMatkul() throws Exception{
        HashMap<String, String> bodyReq = new HashMap<>();
        bodyReq.put("kodeMatkul", "APA");
        bodyReq.put("npm", "1234");
        
        when(enrollService.enrollMataKuliah("APA", "1234")).thenReturn(mhs);
        mvc.perform(post("/enroll")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
                .andExpect(jsonPath("$.npm").value("1234"));
    }

    @Test
    public void testControllerEnrollMatkulInvalidBody() throws Exception{
        HashMap<String, String> bodyReq = new HashMap<>();
        mvc.perform(post("/enroll")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerEnrollMatkulMhsAlreadyEnrolled() throws Exception{
        HashMap<String, String> bodyReq = new HashMap<>();
        bodyReq.put("kodeMatkul", "APA");
        bodyReq.put("npm", "1234");
        mvc.perform(post("/enroll")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(bodyReq)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerListPendaftarMatkul() throws Exception{
        ArrayList<Mahasiswa> mockRet = new ArrayList<>();
        mockRet.add(mhs);

        when(enrollService.pendaftarMataKuliah("KuMenangis")).thenReturn(mockRet);
        mvc.perform(get("/enroll/KuMenangis").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npm").value("1234"));
    }

    @Test
    public void testControllerListPendaftarMatkulMatkulNotExist() throws Exception{
        when(enrollService.pendaftarMataKuliah("KuMenangis")).thenReturn(null);
        mvc.perform(get("/enroll/KuMenangis").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
