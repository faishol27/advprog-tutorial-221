# Notes Tutorial 5
1. Membuat model Log dengan detail sebagai berikut:
    ```
    idLog: integer(Primary Key, auto increment)
    start: datetime
    end: datetime
    deskripsi: Text
    ```
2. Membuat relasi Mahasiswa mendaftar MataKuliah.
3. Membuat relasi Mahasiswa memiliki Log.
4. Membuat endpoint agar Mahasiswa dapat mendaftar MataKuliah dan melihat MataKuliah yang dipilih.
5. Membuat endpoint untuk melihat daftar Mahasiswa yang mendaftar MataKuliah.
6. Membuat agar endpoint DELETE MataKuliah tidak dapat menghapus MataKuliah yang sudah memiliki Mahasiswa.
7. Membuat RESTController untuk relation Mahasiswa dan Log.
8. Membuat endpoint untuk melihat laporan pembayaran Mahasiswa pada rentang bulan tertentu dengan detail berikut:
    ```json
    [
        {
            "Month":"January",
            "jamKerja":10,
            "Pembayaran":3500
        },
        {
            "Month":"February",
            "jamKerja":40,
            "Pembayaran":14000
        }
    ]
    ```
9. Membuat endpoint untuk mendapat summary jumlah uang yang didapat oleh Mahasiswa.

Notes:
- Gaji asisten 350 Greil per jam
- Jam kerja pada laporan dalam satuan jam
- Pembayaran dalam satuan Greil