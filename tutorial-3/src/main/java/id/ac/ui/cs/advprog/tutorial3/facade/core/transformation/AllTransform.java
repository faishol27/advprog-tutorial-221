package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.*;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.*;

import java.util.ArrayList;

public class AllTransform implements Transformation{
    private ArrayList<Transformation> transformAlgo = new ArrayList<>();

    public AllTransform() {
        transformAlgo.add(new AbyssalTransformation());
        transformAlgo.add(new Rot13Transformation());
        transformAlgo.add(new CelestialTransformation());
    }

    public Spell encode(Spell spell){
        for (Transformation algo:transformAlgo) {
            spell = algo.encode(spell);
        }
        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    public Spell decode(Spell spell){
        int sz = transformAlgo.size();
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        for (int i = sz - 1; i >= 0; i--) {
            Transformation algo = transformAlgo.get(i);
            spell = algo.decode(spell);
        }
        return spell;
    }
}
