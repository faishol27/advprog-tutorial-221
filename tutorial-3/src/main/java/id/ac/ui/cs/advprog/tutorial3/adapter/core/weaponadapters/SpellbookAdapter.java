package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// DONE: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isLastStrong = false;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        isLastStrong = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (isLastStrong) {
            return "[Spellbook] Tidak dapat menembakkan dua spell kuat secara beruntun";
        }
        isLastStrong = true;
        return spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
