package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan ROT13 cipher
*/
public class Rot13Transformation implements Transformation{
    public Rot13Transformation() { }

    public Spell encode(Spell spell){
        return process(spell);
    }

    public Spell decode(Spell spell){
        return process(spell);
    }

    private Spell process(Spell spell){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            int chrIdx = codex.getIndex(text.charAt(i));
            if ((11 <= chrIdx && chrIdx < 13 + 11) || (26 + 11 <= chrIdx && chrIdx < 39 + 11)) {
                chrIdx += 13;
            } else if ((13 + 11 <= chrIdx && chrIdx < 26 + 11) || (39 + 11 <= chrIdx && chrIdx < 26*2 + 11)) {
                chrIdx -= 13;
            }
            res[i] = codex.getChar(chrIdx);
        }

        return new Spell(new String(res), codex);
    }
}
