package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

// DONE: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    // DONE: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> ret = weaponRepository.findAll();
        for (Bow bow:bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                ret.add(new BowAdapter(bow));
            }
        }
        for (Spellbook spell:spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spell.getName()) == null) {
                ret.add(new SpellbookAdapter(spell));
            }
        }
        return ret;
    }

    // DONE: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon attacker = null;
        if (weaponRepository.findByAlias(weaponName) != null) {
            attacker = weaponRepository.findByAlias(weaponName);
        } else {
            if (bowRepository.findByAlias(weaponName) != null) {
                Bow tmp = bowRepository.findByAlias(weaponName);
                attacker = new BowAdapter(tmp);
            } else if (spellbookRepository.findByAlias(weaponName) != null) {
                Spellbook tmp = spellbookRepository.findByAlias(weaponName);
                attacker = new SpellbookAdapter(tmp);
            }
        }
        
        weaponRepository.save(attacker);

        String log = attacker.getHolderName() + " attacked with " + attacker.getName();
        if (attackType == 0) {
            log = log + " (normal attack): " + attacker.normalAttack();
        } else {
            log = log + " (charged attack): " + attacker.chargedAttack();
        }

        logRepository.addLog(log);
    }

    // DONE: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
