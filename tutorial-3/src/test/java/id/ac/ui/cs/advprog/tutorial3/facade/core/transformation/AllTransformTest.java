package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AllTransformTest {
    private Class<?> allTransformClass;

    @BeforeEach
    public void setUp() throws Exception {
        allTransformClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AllTransform");
    }

    @Test
    public void testAllTransformIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(allTransformClass.getModifiers()));
    }

    @Test
    public void testAllTransformOverrideEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = allTransformClass.getDeclaredMethod("encode", encodeArgs);
        int methodModifiers = encode.getModifiers();
        
        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                encode.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, encode.getParameterCount());
    }

    @Test
    public void testAllTransformOverrideDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = allTransformClass.getDeclaredMethod("decode", decodeArgs);
        int methodModifiers = decode.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                decode.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, decode.getParameterCount());
    }
}
