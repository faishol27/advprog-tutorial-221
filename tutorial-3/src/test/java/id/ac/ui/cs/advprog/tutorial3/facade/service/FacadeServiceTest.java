package id.ac.ui.cs.advprog.tutorial3.facade.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class FacadeServiceTest {
    private Class<?> facadeServiceClass;

    @InjectMocks
    private FacadeServiceImpl facadeService;

    @BeforeEach
    public void setup() throws Exception {
        facadeServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.service.FacadeServiceImpl");
    }

    @Test
    public void testFacadeServiceHasEncodeMethod() throws Exception {
        Method encode = facadeServiceClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceEncodeCorrectlyImplemented() {
        // DONE: ganti assertion output untuk menguji hasil encode
        String result1 = facadeService.encode("the quick brown fox jumps over the lazy dog");
        assertEquals("M_@A)aS%/bS)!>w_NNdn_$AB]FS}vM|Z^()=FFH__^M", result1);
        String result2 = facadeService.encode("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG");
        assertEquals("r_LtJ>yc/,yJKSwaee/._xtw]iyd;r|=vmJ=iip_avr", result2);
        String result3 = facadeService.encode("1234567890");
        assertEquals("]&<w&%#;;(", result3);
    }

    @Test
    public void testFacadeServiceHasDecodeMethod() throws Exception {
        Method decode = facadeServiceClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceDecodeCorrectlyImplemented() {
        // DONE: ganti input dan assertion output untuk menguji hasil decode
        String result1 = facadeService.decode("M_@A)aS%/bS)!>w_NNdn_$AB]FS}vM|Z^()=FFH__^M");
        assertEquals("the quick brown fox jumps over the lazy dog", result1);
        String result2 = facadeService.decode("r_LtJ>yc/,yJKSwaee/._xtw]iyd;r|=vmJ=iip_avr");
        assertEquals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG", result2);
        String result3 = facadeService.decode("]&<w&%#;;(");
        assertEquals("1234567890", result3);
    }

    @Test
    public void testFacadeServiceHasSetRequestTypeMethod() throws Exception {
        Method setRequestType = facadeServiceClass.getDeclaredMethod("setRequestType", String.class);
        int methodModifiers = setRequestType.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasSetRequestValueMethod() throws Exception {
        Method setRequestValue = facadeServiceClass.getDeclaredMethod("setRequestValue", String.class);
        int methodModifiers = setRequestValue.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasIsRequestEncodeMethod() throws Exception {
        Method isRequestEncode = facadeServiceClass.getDeclaredMethod("isRequestEncode");
        int methodModifiers = isRequestEncode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasGetRequestValueMethod() throws Exception {
        Method getRequestValue = facadeServiceClass.getDeclaredMethod("getRequestValue");
        int methodModifiers = getRequestValue.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
