package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Rot13TransformationTest {
    private Class<?> rot13Class;

    @BeforeEach
    public void setUp() throws Exception {
        rot13Class = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Rot13Transformation");
    }

    @Test
    public void testRot13IsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(rot13Class.getModifiers()));
    }

    @Test
    public void testRot13IsATransformation() {
        Collection<Type> interfaces = Arrays.asList(rot13Class.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation")));
    }

    @Test
    public void testRot13OverrideEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = Spell.class;
        Method encode = rot13Class.getDeclaredMethod("encode", encodeArgs);
        int methodModifiers = encode.getModifiers();
        
        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                encode.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, encode.getParameterCount());
    }

    @Test
    public void testRot13OverrideDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = Spell.class;
        Method decode = rot13Class.getDeclaredMethod("decode", decodeArgs);
        int methodModifiers = decode.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell",
                decode.getGenericReturnType().getTypeName());
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, decode.getParameterCount());
    }

    @Test
    public void testRot13DecodeCorrectlyImplemented() throws Exception {
        Rot13Transformation rot13 = new Rot13Transformation();
        String txt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        Spell spell = new Spell(txt, AlphaCodex.getInstance());
        
        String expRes = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM1234567890";
        assertEquals(expRes, rot13.decode(spell).getText());
    }

    @Test
    public void testRot13EncodeCorrectlyImplemented() throws Exception {
        Rot13Transformation rot13 = new Rot13Transformation();
        String txt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        Spell spell = new Spell(txt, AlphaCodex.getInstance());
        
        String expRes = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM1234567890";
        assertEquals(expRes, rot13.encode(spell).getText());
    }
}
