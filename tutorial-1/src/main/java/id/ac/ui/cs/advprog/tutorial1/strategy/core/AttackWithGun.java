package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //DONE: Complete AttackWithGun
    public String attack() {
        return "Peww";
    }
    public String getType() {
        return "gun";
    }
}
