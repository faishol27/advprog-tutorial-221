package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //DONE: Complete DefendWithBarrier
    public String defend() {
        return "### Protected ###";
    }
    public String getType() {
        return "barrier";
    }
}
