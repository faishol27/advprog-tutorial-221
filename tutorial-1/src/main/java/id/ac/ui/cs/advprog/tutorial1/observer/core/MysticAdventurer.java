package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //DONE: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    //DONE: Complete Me
    public void update() {
        if (this.guild.getQuestType().startsWith("E") || this.guild.getQuestType().startsWith("D")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
