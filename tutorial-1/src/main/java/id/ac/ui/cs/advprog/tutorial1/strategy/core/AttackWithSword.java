package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //DONE: Complete AttackWithSword
    public String attack() {
        return "Swoosh";
    }
    public String getType() {
        return "sword";
    }
}
