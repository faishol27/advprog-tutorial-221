package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //DONE: Complete DefendWithShield
    public String defend() {
        return "$$$ S.H.I.E.L.D $$$";
    }
    public String getType() {
        return "shield";
    }
}
