package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //DONE: Complete DefendWithArmor
    public String defend() {
        return "((( Unbeatable )))";
    }
    public String getType() {
        return "armor";
    }
}
