package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //DONE: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    //DONE: Complete Me
    public void update() {
        if (this.guild.getQuestType().startsWith("R") || this.guild.getQuestType().startsWith("D")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
