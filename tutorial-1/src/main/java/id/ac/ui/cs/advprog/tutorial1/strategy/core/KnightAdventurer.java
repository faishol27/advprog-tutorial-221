package id.ac.ui.cs.advprog.tutorial1.strategy.core;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;

public class KnightAdventurer extends Adventurer {
    //DONE: Implement getAlias() in KnightAdventurer
    private final StrategyRepository strategyRepo = new StrategyRepository();

    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "KNG";
    }
}
