package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //DONE: Complete AttackWithMagic
    public String attack() {
        return "Wooshh";
    }
    public String getType() {
        return "magic";
    }
}
