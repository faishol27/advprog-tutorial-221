package id.ac.ui.cs.advprog.tutorial1.strategy.core;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;

public class AgileAdventurer extends Adventurer {
    //DONE: Implement getAlias() in AgileAdventurer
    private final StrategyRepository strategyRepo = new StrategyRepository();

    public AgileAdventurer() {
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "AGL";
    }
}
