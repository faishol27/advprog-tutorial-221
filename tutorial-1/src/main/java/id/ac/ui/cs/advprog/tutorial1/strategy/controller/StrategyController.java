package id.ac.ui.cs.advprog.tutorial1.strategy.controller;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.AttackBehavior;
import id.ac.ui.cs.advprog.tutorial1.strategy.core.DefenseBehavior;
import id.ac.ui.cs.advprog.tutorial1.strategy.service.AdventurerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/adventurer")
public class StrategyController {

    @Autowired
    private AdventurerService adventurerService;

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public String strategyHome(Model model) {
        Iterable<AttackBehavior> attackBehaviors = adventurerService.getAttackBehaviors();
        Iterable<DefenseBehavior> defenseBehaviors = adventurerService.getDefenseBehaviors();
        //DONE: Complete strategyHome
        model.addAttribute("attackBehaviors", attackBehaviors);
        model.addAttribute("defenseBehaviors", defenseBehaviors);
        model.addAttribute("adventurers", adventurerService.findAll());

        return "strategy/home";
    }

    //DONE: Fill with a correct method
    @RequestMapping(path = "/change-strategy", method = RequestMethod.POST)
    public String changeAttack(
            @RequestParam(value = "alias") String alias,
            @RequestParam(value = "attackType") String attackType,
            @RequestParam(value = "defenseType") String defenseType) {

        adventurerService.changeStrategy(alias, attackType, defenseType);
        //DONE: This should redirect to home URL
        return "redirect:/adventurer/all";
    }

}
