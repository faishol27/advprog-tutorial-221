package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // DONE: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell elm:spells) {
            elm.cast();
        }
    }

    @Override
    public void undo() {
        int sz = spells.size();
        for (int i = sz - 1; i >= 0; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
